# EBAME tutorial on metabolic network reconstruction and modelling - from the individual to the community

## Table of contents

[[_TOC_]]

This tutorial aims at guiding you on the way to using genome-scale metabolic networks and subsequent modelling to analyse the functional potential of microbial communities [1]. It will provide resources to reconstruct metabolic networks, and step-to-step tutorials to analyse them and build your first models using reasoning-based approaches.

## Technical requirements for the tutorial

We will be using a number of bioinformatic tools during this tutorial.
If you were provided with a dedicated VM, hurray!, you have nothing to do, except [deploy a VM](https://biosphere.france-bioinformatique.fr/catalogue/appliance/223/) (4 Gb memory should be ok), and connect to it with ssh.

If you were not, don't fall into despair, there are conda env recipes in this repository and all the remaining commands are listed below.

- **Non-VM users**
    - clone the gitlab project of this tutorial

    ```sh
    mkdir -p ebame_tutorial
    cd ebame_tutorial
    git clone https://gitlab.inria.fr/cfrioux/ebame.git
    ```

    - create the following conda environments

    ```sh
    conda env create -f ebame/conda/ebame_metabo_gapseq.yml
    conda env create -f ebame/conda/ebame_metabo_reasoning.yml
    ```

    - finish installing the gapseq dependencies

    ```sh
    # activate the gapseq conda env
    conda activate ebame_metabo_gapseq
    # install missing R package
    R -e 'install.packages("CHNOSZ", repos="http://cran.us.r-project.org")'

    # Download & Install R-package 'sybilSBML'
    wget https://cran.r-project.org/src/contrib/Archive/sybilSBML/sybilSBML_3.1.2.tar.gz
    R CMD INSTALL --configure-args=" \
    --with-sbml-include=$CONDA_PREFIX/include \
    --with-sbml-lib=$CONDA_PREFIX/lib" sybilSBML_3.1.2.tar.gz
    rm sybilSBML_3.1.2.tar.gz
    ```

    - clone gapseq and update gapseq databases

    ```sh
    conda activate ebame_metabo_gapseq
    # clone gapseq in the ebame directory
    git clone https://github.com/jotech/gapseq.git
    # Download reference sequence data
    bash ./gapseq/src/update_sequences.sh
    ```

    - download the jar file dependency for metage2metabo

    ```sh
    wget https://github.com/AuReMe/metage2metabo/raw/main/external_dependencies/Oog_CommandLineTool2012/Oog.jar
    ```

    - install the latest version of Menetools and Kegg2Bipartite

    ```sh
    conda deactivate
    conda activate ebame_metabo_reasoning
    pip install git+https://github.com/ArnaudBelcour/kegg2bipartitegraph
    ```

At this step, you should have the following data structure in the `ebame_tutorial` directory:

```txt
ebame_tutorial
├── ebame
│   ├── README.md
│   ├── conda
│   │   ├── ebame_metabo_gapseq.yml
│   │   └── ebame_metabo_reasoning.yml
│   ├── data
│   │   ├── Escherichia_coli
│   │   │   └── e_coli_core.xml
│   │   ├── Mycoplasma_genitalium
│   │   │   ├── EggNOG_annotation
│   │   │   │   └── MM_9sz71w4a.emapper.annotations.tsv
│   │   │   ├── KEGG_metabolic_network
│   │   │   │   └── kegg_network.tar.gz
│   │   │   ├── gapseq_outputs
│   │   │   │   └── ...
│   │   │   ├── genome
│   │   │   │   └── GCA_000027325.1_ASM2732v1_genomic.fna
│   │   │   └── proteome
│   │   │       └── GCF_000027325.1.faa
│   │   └── oceanDNA
│   │       ├── genomes
│   │       │   └── ...
│   │       ├── metabolic_networks
│   │       │   ├── carveme
│   │       │   │   └── sbml
│   │       │   │       └── ...
│   │       │   └── pathwaytools
│   │       │       ├── padmet
│   │       │       │   └── ...
│   │       │       └── sbml
│   │       │           └── ...
│   │       ├── seeds_ocean_dna_ptools.sbml
│   │       ├── selected_genomes.tsv
│   │       └── taxon_id.tsv
│   ├── databases
│   │   └── metacyc25-5.padmet
│   ├── figures
│   │   └── ...
│   ├── results
│   │   └── readme.md
│   ├── scripts
│   │   └── community_creation_sbml.py
│   └── toys
│       ├── 6_bact_community
│       │   └── ...
│       ├── metaorganism.sbml
│       ├── seeds_community.sbml
│       ├── seeds_empty.sbml
│       ├── targets_community.sbml
│       └── targets_individual.sbml
├── gapseq
│    ├── ...
│    ├── gapseq
│    └── ...
└── Oog.jar
```

<a name="metabolic_networks"></a>

## A metabolic... what? 

When not familiar with the concept of genome-scale metabolic network, it can all feel a bit abstract.
Let's take a step back here to take a look at the object, manipulate it a bit, and explore its widely-used exchange format, the [SBML](https://sbml.org/) [2].

Metabolic networks are sets of metabolic reactions consuming substrates and producing other molecules from it. Genome-scale metabolic networks include the reactions predicted from the entire genome of an organism [1]. The link between genes and metabolic reactions occurs through gene-protein-reaction relationships (GPR): enzymes encoded in the genome catalyse reactions. Therefore the annotation of proteins - both structural i.e. finding the genes, and functional, i.e. associating functions - is the basis of metabolic network reconstruction at genome scale. In practice, proteomes are annotated, associated functions through various types of ontology (e.g EC numbers, GO terms...) and dedicated tools associate biochemical reactions to these annotations using knowledge bases.

<a name="metabolic_networks_fluxer"></a>

### Manipulation of metabolic models with Fluxer 

That's it, not even 10 lines of tutorial and we end up with two terms "metabolic network" and "metabolic model". A *metabolic network* is the (oriented) graph object: a set of nodes (e.g. metabolites), connected by (hyper)edges (reactions) where incoming edges to a metabolite denote production and outgoing edges denote consumption.

A *metabolic model* is a metabolic network combined to a mathematic formalism (e.g. numerical constraints) for simulation.

What do metabolic networks look like? Visualising them is a complex task as they usually include a few thousands metabolites and reactions at genome-scale. We will nonetheless start the tutorial by a visual exploration to grasp the concepts underlying the object.

[Fluxer](https://fluxer.umbc.edu/) [3] is an online tool for visualising genome-scale metabolic models. The interface is interactive, enabling us to actually manipulate the model and better understand the underlying concepts.

In Fluxer the objects we manipulate are metabolic models because they account not only for the structure of the metabolic network, but also for simulation results depicting the activity of metabolic reactions under the constraint of an objective function's maximisation -- usually the biomass. Details about these simulations will be provided in the tutorial of Damien Eveillard. Such activity will be indicated by values and colorations related to the edges of the network, we can ignore it for now.

1. Go to [Fluxer website](https://fluxer.umbc.edu/)
2. Pick "Escherichia coli str. K-12 substr. MG1655 - e_coli_core" in `View an organism model` > `Select model`
3. By default, the visualisation is driven by an algorithm that simplifies the network into a "spanning tree" to make it more human-readable. You can play the options to visualise the full, densely connected network ("complete graph"), change the layout to one that is more usual for graphs ("force"), and display all molecules occurring in the network that were previously hidden ("option / cofactor nodes > show").
4. Go back to the Homepage of Fluxer website and pick a real-size genome-scale model. You can pick `Homo sapiens - Recon3D` if you feel confident in the computer's ability to load such a large model (5835 metabolites, 10600 reactions), otherwise pick a random organism.
5. Display the complete graph and see the limits of visualisation when it comes to genome-scale metabolic networks: we need bioinformatics tools to proporly understand the simulated behaviour of organisms in environmental conditions.

Visualisation is helpful but the size of metabolic networks limit the information we can extract from them. We'll need command lines to go further and simulate the behaviour of organisms in given environmental conditions. Let's now take a look at the main format used for exchanging metabolic networks and models: the systems biology markup language.

<a name="metabolic_networks_sbml"></a>

### A closer look at a metabolic network: the SBML 

SBML (System Biology Markup Language) is the most widely-used language to share metabolic networks and models. It is both computer and human-readable. We will now look at all the information related to metabolism and its simulation that SBML can store.

1. Look for the SBML file of _E. coli_ core in this repository `ebame/data/Escherichia_coli/e_coli_core.xml` or directly download it from the [Bigg database]((http://bigg.ucsd.edu/models/e_coli_core)) [4].
2. Open the file using you favourite text editor or one that you have available in your computing set-up (an example: [VS Code](https://code.visualstudio.com)).
3. Observe the components of the metabolic network:
    - `listOfCompartments` describes the compartments of the system. It usually includes the cytosolic and extracellular compartments. Depending on the organism and the precision of the model, it can include additional ones (periplasm, nucleus, mitochondria...)
    - `listOfSpecies` describes the metabolites of the network. Each of them is at least described by a unique identifier (alpha-numerical characters + "\_", cannot start with a number) and usually has a name, a compartment, the associated chemical formula... By convention, a metabolite identifier starts with a "M_" and its suffix is the identifier of the compartment, preceded by an underscore character. For instance, take a look at the identifier "M_gln__L_c": it is a metabolite ("M_") from the cytosol "_c", the L-glutamine ("gln__L"). The core metabolism of *E. coli* has 72 metabolites.  
    - `fbc:listOfGeneProducts` describes all the genes associated to metabolic reactions. Each gene has an identifier (prefix "G_"), a name and several external references. 137 genes are referred to by metabolic reactions in the core metabolism of *E. coli*.
    - `listOfReactions` describes the metabolic reactions, i.e. biochemical transformations of substrates into products, as well as transport reactions. The core metabolism of *E. coli* has 95 of them. Important fields in the description of the reaction include the identifier ("R_" prefix), its name, the reversibility status of the reaction (false: goes forwards, true: goes forwards and backwards), and its bounds for flux simulation (see below). Each reaction has a description of the substrates that are consumed and the metabolites that are produced.
        - `listOfReactants` describes the substrates of the reaction, one by line in `speciesReference`. The content of the field named `species` refers to a metabolite identifier of `listOfSpecies`. The content of the field named `stoichiometry` refers to the number of molecules involved as reaction substrates.
        - `listOfProducts` describes the products of the reaction, one by line with the associated stoichiometry.
        - `fbc:geneProductAssociation` lists the genes associated to the reaction, referred to as "gene-protein-reaction" (GPR) relationship.

        The "R_PFL" reaction is the non-reversible "Pyruvate formate lyase" transformation. It consumes one molecule of cytosolic co-enzyme A ("M_coa_c") and one molecule of cytolosil pyruvate ("M_pyr"). It produces one molecule of cytosolic acetyl-coA ("M_accoa_c") and one molecule of cytosolic formate ("M_for"). The presence of the reaction is conditioned by the expression of genes, expressed as a rule: "((b0902 and b0903) and b2579) or (b0902 and b0903) or (b0902 and b3114) or (b3951 and b3952)"

        There are specific reactions in the list:

        - The **biomass reaction** that is not a biochemical reaction but rather a modeller's abstraction to the requirements of the organisms for growth. "R_BIOMASS_Ecoli_core_w_GAM" is the biomass reaction of *E. coli* core metabolism. If the activation of this reaction can be simulated using dedicated mathematical models such as Flux Balance Analysis, it predicts that the bacterium could grow in the simulated environmental conditions. 
        - **Exchange reactions**, denoted with the predix "R_EX_". Those reactions have no product. They denote the export and/or import of metabolites from/into the system. The reversibility and bounds of the reaction inform us on its purpose: export, and or import. For more details about these reactions, and other types of "boundary reactions", refer to the documentation of [cobrapy](https://cobrapy.readthedocs.io/en/latest/building_model.html#Exchanges,-Sinks-and-Demands) the widely used Python package for manipulation and simulation of metabolic models. For instance, let's look at the reaction "R_EX_glc__D_e": `1 M_glc__D_e <--> `. It can both import glc into the system (producing it from nothing in the backwards direction) or export it (consuming it and producing nothing in the forwards direction).
4. There is additional relevant information in the SBML for simulation purposes. 
    - `listOfUnitDefinitions` describes the units for flux calculation, usually millimol per gram of dry weight per hour.
    - `fbc:listOfObjectives` describes the metabolic objective, i.e. the objective function for flux simulation, here the biomass reaction that abstracts the metabolic requirements for growth. You can go check its substrates and products by looking for `R_BIOMASS_Ecoli_core_w_GAM` in the list of reactions. 
    - `listOfParameters` describes the flux constraints of reactions, lower and upper bounds that flux cannot exceed during simulations. 

## Metabolic network reconstruction <a name="metabolic_reconstruction"></a>

### Standalone tools <a name="metabolic_reconstruction_tools"></a>

For the sake of computation time and/or because some tools are not freely available to all, the metabolic networks or intermediary files used in this tutorial will be provided already, there is no need to generate them.

Nonetheless, below is a list of tools one can install and apply to reconstructed metagenome-assembled genome (MAGs) or any reference genome.

- **carveme** ([code](https://github.com/cdanielmachado/carveme/), [doc](https://carveme.readthedocs.io/en/latest/)) [5] is probably the fastest and easiest tool for metabolic network reconstruction for microbes. The tool has curated universes of metabolic reactions for groups of organisms (Gram-positive and Gram-negative bacteria, archaea and cyanobacteria). The algorithm carves the relevant universal model to the genome provided by the user by solving a mixed integer linear programme (MILP).
    - *Installation*: easy with `python pip`
    - *Specific requirements*: an IBM CPLEX Optimizer license. [Freely available to academics](http://ibm.biz/CPLEXonAI). Alternatively, a [Gurobi Optimizer license](https://www.gurobi.com/downloads/gurobi-software/)
    - *Input*: a proteome in fasta. Can be generated from a genome using Prodigal (see below)
    - *Basic usage*: `carve genome.faa --output model.xml` 
    - *Runtime*: a few minutes
- **gapseq** ([code](https://github.com/jotech/gapseq), [doc](https://gapseq.readthedocs.io/en/latest/index.html)) [6] can be used to generate genome-scale metabolic networks (`doall` command) and or retrieve the list of metabolic pathways, transporters (`find all` command) from a genome.
    - *Installation*: relatively easy in a conda environment. Don't forget to [update the databases](https://gapseq.readthedocs.io/en/latest/install.html).
    - *Specific requirements*: gapseq solves a MILP programme as well. It can use a free solver (glpk) but the performances increase with the IBM CPLEX solver. 
    - *Input*: a genome, alternatively a proteome for the inference of pathways and reactions
    - *Basic usage*: 
        - find pathways and transporters: `./gapseq find -p all data/oceanDNA/genomes/OceanDNA-b13538.fna.gz`
        - reconstruct the metabolic model from a genome or a proteome: `./gapseq doall data/oceanDNA/genomes/OceanDNA-b13538.fna.gz`
    - *Runtime*: up to a few hours
- **Pathway Tools** ([download](http://bioinformatics.ai.sri.com/ptools/)) is an interface-based programme for the reconstruction of pathway-genome databases (PGDB), i.e. metabolic networks [7]. The software is freely available for academics. It relies on a clickable user interface, which is valuable to get familiar with the tool but can be troublesome for the generation of multiple metabolic networks, which can occur in the context of metagenomics. In that case, a Python wrapper can be used: mpwt ([code and doc](https://github.com/AuReMe/mpwt)) [21] to manage Pathway Tools in a command-line manner. Pathway Tools requires as input annotated (both structurally and functionnally) genomes, ideally with GO-terms and or EC-numbers. Structural annotation can be done with Prodigal (see below). Functional annotation can be performed with EggNOG-mapper ([online server](http://eggnog-mapper.embl.de/), [code](https://github.com/eggnogdb/eggnog-mapper/), [doc](https://github.com/jhcepas/eggnog-mapper/wiki)). Dedicated genbank input for Pathway Tools can be generated using [emapper2gbk](https://github.com/AuReMe/emapper2gbk).
    - *input*: functionally-annotated genome
    - *runtime*: usually less than one hour

### Online resources <a name="metabolic_reconstruction_online"></a>

#### For reconstruction <a name="metabolic_reconstruction_online_tools"></a>

- [Kbase](https://www.kbase.us/) ([doc](https://docs.kbase.us/)) is an online platform for bioinformatics analyses [8]. Among others, it permits the reconstruction of metabolic networks. A lot of analyses are available in Kbase [catalogue](https://narrative.kbase.us/#catalog/apps) (e.g. type "metabolic" in the search field), including the creation of a [metabolic model for a metagenome](https://narrative.kbase.us/#catalog/apps/fba_tools/build_metagenome_model/release) using ModelSEED.

#### Databases of metabolic reconstructions <a name="metabolic_reconstruction_online_db"></a>

- [Bigg](http://bigg.ucsd.edu/models) - 108 metabolic models that were manually curated [4].
- [Biocyc](https://biocyc.org/) - More than 20,000 metabolic reconstructions, among which 5 highly curated (Tier 1), 67 curated (Tier 2) and the rest automatically reconstructed using Pathway Tools. Data is freely accessible for academics upon subscription. The free academic account does not provide up-to-date Tier 3 reconstructions but rather PGDBs obtained with a slightly older version of Pathway Tools. [9]
- [VMH](https://www.vmh.life/#home) - 818 metabolic reconstructions of microbes from the human gut microbiome. [10]
- AGORA2 ([data](https://www.vmh.life/files/reconstructions/AGORA2/), [paper](https://doi.org/10.1038/s41587-022-01628-0)) - 7,302 metabolic reconstructions for human-associated micro-organisms. [11]
- [Biomodels](https://www.ebi.ac.uk/biomodels/) - a database of mathematical models of biological and biomedical systems including metabolic reconstructions.[12]

### Structural annotation of a genome with Prodigal

Prodigal is used to detect genes in prokaryotic genomes [13]. We will try it on one genome in this repository.

- *Installation*: [conda](https://anaconda.org/bioconda/prodigal): `conda install -c bioconda prodigal` [do not run, it is already installed in a conda env of this tutorial]
- *Usage*:

    ```sh
    # activate the env in which it is installed
    conda activate ebame_metabo_reasoning
    # unzip the genome
    gunzip ebame/data/oceanDNA/genomes/OceanDNA-b13538.fna.gz
    # run prodigal
    prodigal -i ebame/data/oceanDNA/genomes/OceanDNA-b13538.fna -d ebame/results/OceanDNA-b13538.fna -a ebame/results/OceanDNA-b13538.faa
    ```

    Look at the input and output files into `/results`. 

### Tutorial - Metabolic reconstruction with gapseq

Running the full reconstruction of a metabolic network with gapseq would take too much time in this tutorial. Therefore some steps were performed previously and their outputs are available in this repository. We can however make some tests and query some pathways as well as performing transporter annotation on a genome.

The outputs of several gapseq commands are in `ebame/data/Mycoplasma_genitalium/gapseq_outputs`.

For this part, we will use the gapseq conda environment:

```sh
conda activate ebame_metabo_gapseq
```

1. Let's look at the predictions of pathways and reactions for a small genome, the one of *Mycoplasma genitalium*. It takes some time to generate the files so **do not run the command below** and directly take a look at the results in `ebame/data/Mycoplasma_genitalium/gapseq_outputs`.
The following commands `./gapseq/gapseq find -p all -t Bacteria -M prot -K 10 data/Mycoplasma_genitalium/proteome/GCF_000027325.1.faa` produces `GCF_000027325.1-all-Pathways.tbl` and `GCF_000027325.1-all-Reactions.tbl`. 
2. Let's predict the transporters from the genomes, using the following command.

    ```sh
    ./gapseq/gapseq find-transport -M prot  ebame/data/Mycoplasma_genitalium/proteome/GCF_000027325.1.faa
    ```

    It produces the following file `GCF_000027325.1-Transporter.tbl`. Take a look at the output.
3. Look for the pathways and reactions related to "lipoate" using the following command.

    ```sh
    ./gapseq/gapseq find -p lipoate ebame/data/Mycoplasma_genitalium/proteome/GCF_000027325.1.faa
    ```

    It produces the following files `GCF_000027325.1-lipoate-Pathways.tbl` and `GCF_000027325.1-lipoate-Reactions.tbl`. Take a look at the output.
4. From the transporters, the reactions and the pathways, we can obtain a draft metabolic network with the following command:

    ```sh
    ./gapseq/gapseq draft -r ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1-all-Reactions.tbl -t ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1-Transporter.tbl -p ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1-all-Pathways.tbl -c ebame/data/Mycoplasma_genitalium/proteome/GCF_000027325.1.faa
    ```

    This creates the following files `./GCF_000027325.1-draft.xml`, `GCF_000027325.1-draft.RDS`, `GCF_000027325.1-rxnWeights.RDS` and `GCF_000027325.1-rxnXgenes.RDS`. The `.xml` file is a SBML file of the metabolic network. 
5. We can already start working with the draft. However, the final step to a complete metabolic network consists in the gap-filling procedure, that will add missing reactions in the model in order to ensure the activation of the biomass reaction during simulations. This step requires the outputs of the previous ones together with a description of the medium the organism can grow in. Let's take a random medium available in the package for this tutorial. This can be done with the following command:

    ```sh
    ./gapseq/gapseq fill -m GCF_000027325.1-draft.RDS -c GCF_000027325.1-rxnWeights.RDS -g GCF_000027325.1-rxnXgenes.RDS -n gapseq/dat/media/TSBmed.csv
    ```

    This step is much faster using the CPLEX solver. It creates the following files `GCF_000027325.1.RDS` and `GCF_000027325.1.xml`. 

### Metabolic network reconstruction using the KEGG Database and kegg2bipartite

For this part, we will switch the conda environment:

```sh
conda activate ebame_metabo_reasoning
```

1. Using the [EggNOG-mapper server](http://eggnog-mapper.embl.de/) [14], functionally annotate the proteome of _M.genitalium_ `data/Mycoplasma_genitalium/proteome/GCF_000027325.1.faa`. Select "data > proteins". Leave the other default options and give your email address to launch the job.
2. Check your emails. Click on "start job" on the interface the emailed link brings you to.
3. Retrieve the output file in `csv` format, or alternatively, use the one in this repository `data/Mycoplasma_genitalium/EggNOG_annotation/MM_9sz71w4a.emapper.annotations.tsv`
4. Use `kegg2bipartitegraph` to create a draft metabolic network of _M.genitalium_ using the EggNOG mapper annotation and the [KEGG database](https://www.genome.jp/kegg/) [15] with the following command:

```sh
k2bg reconstruct_from_eggnog -i ebame/data/Mycoplasma_genitalium/EggNOG_annotation -o ebame/results/KEGG_metabolic_network  
```

1. Alternatively, if you work with well-studied organism, you can directly give the name of the organism as input in order to retrieve all the reactions predicted to be present in this organism according to the KEGG database. The input is the KEGG code for the genome, here "mge" for *Mycoplasma genitalium* G37.

```sh
k2bg reconstruct_from_organism -i mge -o ebame/results/KEGG_metabolic_network_mge/
```

### Quality of the reconstruction

It is quite hard to assess the quality of a reconstructed metabolic network automatically. One can compare the network with biological knowledge, check the gene annotations associated to functions... 
Omic data can be included in the network by constraining the activity of reactions. 

There is a tool for the validation of the metabolic network as a model: MEMOTE [30]. It ensures the standardisation of the models. Documentation is available [online](https://memote.readthedocs.io/en/latest/getting_started.html). It is also possible to use a [webservice](https://memote.io/) to validate individual models.

If you want to test it using the command line (may take a few minutes):

```
pip install memote
```

Let us pick a metabolic model generated by carveme:

```
cp ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml.gz .
gunzip OceanDNA-b15250.sbml.gz
```

And run memote. It will generate a report as a webpage called `index.html`.

```
memote report snapshot OceanDNA-b15250.sbml
```

The generated report is also stored in `ebame/data/oceanDNA/memote/OceanDNA-b15250.html`. It can be opened with a web browser. 


### \[Bonus\] Exploring metabolic networks created with Pathway Tools

One analysis that can be done after the construction of metabolic network is a basis exploration of the pathways present in each one.
In Metacyc, reactions belong to patwhays. Therefore, by comparing the list of reactions to the pathway associations they exhibit in the database, one can get an idea of the pathways present in the network. More precisely, for a given pathway, we can check which of the associated reactions are in the metabolic network, which are not, enabling the computation of a pathway completeness score. 

This is what we will do right now. Metabolic networks for 10 MAGs of the OceanDNA project were reconstructed with Pathway Tools and mpwt. The Pathway Genomes Databases created by Pathway Tools were merged in a single file, of a specific format named [`padmet`](https://padmet.readthedocs.io/en/latest/) [16] that comes with its library. We will use the padmet files of all 10 metabolic networks, and the padmet file of the entire metacyc database to assess the completeness of metabolic pathways present in our networks.

This step involves the use of `padmet report`.
Note that the padmet are compressed by default in this repository. Use `gunzip file.gz` to uncompress a file. 

```sh
gunzip ebame/data/oceanDNA/metabolic_networks/pathwaytools/padmet/OceanDNA-b13538.padmet.gz
padmet report_network --padmetSpec ebame/data/oceanDNA/metabolic_networks/pathwaytools/padmet/OceanDNA-b13538.padmet --padmetRef ebame/databases/metacyc25-5.padmet --output ebame/results/report_OceanDNA-b13538 -v
```

We could automatise the process with a little shell script:

```sh
# create a directory for the reports
OUTDIR="ebame/results/padmet_reports"
mkdir -p $OUTDIR
# first let's gzip again the file we treated juste before
gzip ebame/data/oceanDNA/metabolic_networks/pathwaytools/padmet/OceanDNA-b13538.padmet
# loop over all padmet files, copy the compressed files, get their basename w/o extension, gunzip them, run padmet report, remove them
for elem in ebame/data/oceanDNA/metabolic_networks/pathwaytools/padmet/*.padmet.gz; do cp $elem . && nm=$(basename $elem | cut -d . -f1) && gunzip $nm.padmet.gz && padmet report_network --padmetSpec $nm.padmet --padmetRef ebame/databases/metacyc25-5.padmet --output $OUTDIR $nm && rm $nm.padmet ; done
```

Look at the tables generated for each metabolic network. For instance OceanDNA-b13538:

- `all_genes.tsv` gathers all genes, their name is knowns, and the reaction(s) they are associated to
    | id                 | Common name | linked reactions    |
    |--------------------|-------------|---------------------|
    | EEHN01000001.1_1   | Unknown     | 1.3.99.16-RXN       |
    | EEHN01000001.1_10  | Unknown     | RXN-17357;RXN-21452 |
    | EEHN01000001.1_100 | Unknown     |                     |
    | EEHN01000001.1_101 | Unknown     |                     |
    | EEHN01000001.1_102 | Unknown     |                     |
    | EEHN01000001.1_103 | Unknown     |                     |
    | EEHN01000001.1_104 | Unknown     |                     |
    | EEHN01000001.1_105 | Unknown     |                     |
    | EEHN01000001.1_106 | feoA        |                     |
- `all_metabolites.tsv` gathers the description of all metabolites: identifier, name and whether it is a product (`p`), a substrate (`c`) or both product and substrates of reactions (`cp`)
    | dbRef_id                             | Common name                                                                                       | Produced (p), Consumed (c), Both (cp) |
    |--------------------------------------|---------------------------------------------------------------------------------------------------|---------------------------------------|
    | PHOSPHORIBOSYL-FORMAMIDO-CARBOXAMIDE | 5-formamido-1-(5-phospho-D-ribosyl)-imidazole-4-carboxamide                                       | cp                                    |
    | Peptides-with-Leader-Sequence        | a peptide with a leader sequence                                                                  | c                                     |
    | tRNA-fragment                        | a tRNA fragment                                                                                   | p                                     |
    | PHOSPHORIBOSYL-FORMIMINO-AICAR-P     | 1-(5-phospho-&beta;-D-ribosyl)-5-[(5-phosphoribosylamino)methylideneamino]imidazole-4-carboxamide | cp                                    |
    | tRNA-precursors                      | a tRNA precursor                                                                                  | c                                     |
    | tRNA-pseudouridine-38-40             | a pseudouridine38-40 in tRNA                                                                      | p                                     |
    | ILE                                  | L-isoleucine                                                                                      | cp                                    |
    | PHOSPHORIBULOSYL-FORMIMINO-AICAR-P   | phosphoribulosylformimino-AICAR-phosphate                                                         | cp                                    |
    | biotin-L-lysine-in-BCCP-dimers       | a [biotin carboxyl-carrier-protein dimer]-N6-biotinyl-L-lysine                                    | cp                                    |
- `all_pathways.tsv` gathers all pathways present partly or entirely in the metabolic network. Long story short about the tweaks of this file, if the padmet file is not reconstructed with the entire Metacyc database padmet, then only the pathways described in the Pathway Tools PGDB (`pathways.dat`) are displayed. If one uses the padmet ref in the padmet generation, then any pathway of the database matching the reactions IDs will be added, and their completeness ratios will be calculated. Depending on what you want, both can be relevant. Here we used the second case, this is why some pathways are incomplete and are a bit surprising. For instance, we are studying a bacterium and we find a hit for the "TCA cycle II (plants and fungi)". It occurs because some reactions of this pathway are common with other pathways such as the TCA for bacteria. It wouldn't occur here had the padmet file been generated without the full reference database.
    | dbRef_id         | Common name                                                                              | Taxons                                                    | Number of reaction found | Total number of reaction | Ratio (Reaction found / Total) |
    |------------------|------------------------------------------------------------------------------------------|-----------------------------------------------------------|--------------------------|--------------------------|--------------------------------|
    | PWY-5686         | UMP biosynthesis I                                                                       | TAX-2759;TAX-2                                            | 6                        | 6                        | 1.00                           |
    | PWY-7318         | dTDP-3-acetamido-3,6-dideoxy-&alpha;-D-glucose biosynthesis                              | TAX-2                                                     | 2                        | 5                        | 0.40                           |
    | PWY-6605         | adenine and adenosine salvage II                                                         | TAX-2759;TAX-2                                            | 2                        | 2                        | 1.00                           |
    | TEICHOICACID-PWY | poly(glycerol phosphate) wall teichoic acid biosynthesis                                 | TAX-1239                                                  | 3                        | 12                       | 0.25                           |
    | PWY-8222         | Escherichia coli serotype O:128 O antigen biosynthesis                                   | TAX-1236                                                  | 2                        | 8                        | 0.25                           |
    | PWY-6964         | ammonia assimilation cycle II                                                            | TAX-1117;TAX-2836;TAX-33090                               | 1                        | 2                        | 0.50                           |
    | PWY-8224         | Escherichia coli serotype O:157/Salmonella enterica serotype O:30 O antigen biosynthesis | TAX-2                                                     | 2                        | 7                        | 0.29                           |
    | PWY-6969         | TCA cycle V (2-oxoglutarate synthase)                                                    | TAX-200783;TAX-2157;TAX-1117;TAX-3035;TAX-1224;TAX-201174 | 9                        | 9                        | 1.00                           |
    | PWY-5690         | TCA cycle II (plants and fungi)                                                          | TAX-4751;TAX-33090                                        | 7                        | 9                        | 0.78                           |
- `all_reactions.tsv` gathers information about the reactions presents in the metabolic network: their formula, name, the pathways they are associated to, their origin (always annotation of genes in our case)
    | dbRef_id      | Common name | formula (with id)                                                                                          | formula (with common name)                                                                                           | in pathways       | associated genes  | categories |
    |---------------|-------------|------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|-------------------|-------------------|------------|
    | 1.1.1.127-RXN | NIL         | 1 2-DEHYDRO-3-DEOXY-D-GLUCONATE + 1 NAD <=> 1 CPD-343 + 1 NADH + 1 PROTON                                  | 1 2-dehydro-3-deoxy-D-gluconate + 1 NAD+ <=> 1 3-deoxy-D-glycero-2,5-hexodiulosonate + 1 NADH + 1 H+                 | PWY-6507;PWY-7562 | EEHN01000030.1_9  | ANNOTATION |
    | 1.1.1.136-RXN | NIL         | 1 UDP-N-ACETYL-D-GLUCOSAMINE + 2 NAD + 1 WATER => 1 UDP-N-ACETYL-2-AMINO-D-GLUCURONATE + 2 NADH + 3 PROTON | 1 UDP-N-acetyl-&alpha;-D-glucosamine + 2 NAD+ + 1 H2O => 1 UDP-N-acetyl-&alpha;-D-glucosaminouronate + 2 NADH + 3 H+ | PWY-7090;PWY-7336 | EEHN01000005.1_40 | ANNOTATION |


## Metabolic network modelling <a name="metabolic_modelling"></a>

The exploration of metabolic capabilities we propose in this tutorial is based on the _network expansion_ algorithm, proposed by Ebenhöh et al (2004) [17]. The underlying principle is an iterative computation of the _scope_ of a metabolic network from a set of available nutrients denoted as _seeds_ according to the following rule: the products of a reaction are available if all of its substrates are available. At the first step of the iteration, any reaction whose substrates are all seeds will be activated and its products will be considered producible. At the second step, the products of the reactions activated at the first step can activate new reactions. This keeps going on until a fixed point is reached.

Note that the stoichiometry of the reactions is ignored here. "F" is needed for reaction R3 below, the quantity is disregarded.

The figures below illustrate the concept.
Seeds are the external metabolites. Coloured molecules are in the scope. The animation illustrates the iterations.

<p float="left">
<img src="figures/scope_1.gif" alt="First example of scope computation. A shape depicting a cell with a small metabolic network in it composed of 3 reactions. R1: A + B --> C + D. R2: C --> E. R3: E + 2F -_> G. A and B molecules are also present outside in the extracellular compartment with a transport towards the intracellular. The animation illustrates the scope computation from the seeds A and B. First they activate R1, adding C and D in the scope. Second, C activates R2, adding E to the scope. Then nothing more. A, B, C, D and E are in the scope." title="First example of scope computation. The animation illustrates the scope computation from the seeds A and B. First they activate R1, adding C and D in the scope. Second, C activates R2, adding E to the scope. Then nothing more. A, B, C, D and E are in the scope." width="400"/>  
<img src="figures/scope_2.gif" alt="Second example of scope computation. Same network as in the precedent figure but the seeds change to C, E, F. Now C, E and F are in the extracellular compartments with transports to the intracellular. First and unique step of the scope computation activates R2 through the presence of C, and R3 from the presence of E and F. The scope consists in C, E, F, G." title="Second example of scope computation. First and unique step of the scope computation activates R2 through the presence of C, and R3 from the presence of E and F. The scope consists in C, E, F, G." width="400"/>
</p>

The scope computation of network extension has been used in several implementations. In this tutorial, we use a few tools that implement network expansion for individual metabolic networks, or the extension of network expansion to infer the metabolic potential of a microbial community.

### Using toy data <a name="metabolic_exploration_toy"></a>

For this part, we will work with a toy metabolic network, stored as a SBML file in `toys/metaorganism.sbml`. The script that was used to generate this files is in `scripts/community_creation_sbml.py`. It uses [cobrapy](https://cobrapy.readthedocs.io/en/latest/building_model.html) for the creation and the manipulation of the models [18].

The metabolic network is illustrated below.

<img src="figures/metaorg.png" alt="A representation of a toy metabolic network that is also described as a SBML file in `toys/metaorganism.sbml`" title="A representation of a toy metabolic network that is also described as a SBML file in `toys/metaorganism.sbml`" width="400"/>  

We will use [MeneTools](https://github.com/cfrioux/MeneTools) [16] to explore the metabolic network. The MeneTools is a toolbox enabling the query and exploration of individual metabolic networks. Most tools rely on the network expansion algorithm in order to compute the producible compounds and activable reactions in a metabolic network based on the nutrient composition of the environment. The environment we consider here consists in two metabolites: S1 and S2 (yellow ones in the figure above). Here we will use a few of the MeneTools:

- MeneCheck: checks whether target metabolites could be produced by a metabolic network from the seeds. Here we will check the producibility of F, C and U metabolites from the seeds S1 and S2. Check it manually before running the tool.

    ```sh
    mene check -s ebame/toys/seeds_community.sbml -d ebame/toys/metaorganism.sbml -t ebame/toys/targets_individual.sbml
    ```

- MeneScope: provides the complete list of reachable (i.e. producible metabolites) from a set of seeds based on the network expansion algorithm. What would be producible from S1 and S2 here?

    ```sh
    mene scope -s ebame/toys/seeds_community.sbml -d ebame/toys/metaorganism.sbml
    ```

    MeneScope also gives additional information related to the seeds:

    - which seeds are not found in the metabolic network (can be normal, can highlight a typo in the generation of identifiers)
    - which seeds can be produced by the metabolic network, i.e. seed compounds for which a production path can be activated, enabling a new source for this compound in addition to the one from the environment
    - which seeds are not producible, i.e. they might be limiting substrates
  
    There is an incremental version of MeneScope that presents the produible metabolites at each iteration of the algorithm as described and illustrated above. The results of this computation are displayed in a json file and it is generated this way:

    ```sh
    mene scope_inc -s ebame/toys/seeds_community.sbml -d ebame/toys/metaorganism.sbml --output ebame/results/menescope_inc.json
    ```

- MeneActi focuses on reactions instead of metabolites. It will provide the list of reactions that could be activated in the environment, i.e. reactions whose complete set of reactants are in the scope according to network expansion. 

    ```sh
    mene acti -s ebame/toys/seeds_community.sbml -d ebame/toys/metaorganism.sbml
    ```

- MeneDead looks for structural dead-ends in the metabolic network. It does not take any modelling into account for computation, just the topology of the graph. It informs us on the metabolites that are never substrates of reactions (cannot be consumed), or never products of reactions (cannot be produced). This can be useful to detect potential gaps in the network. For instance, in our toy, `R_7`  is not connected to the rest of the network, `M` is never produced and `Z` never consumed. It it were a real metabolic network at genome-scale, we would expect this reaction to be connected to others. `U` is never produced: we could infer that either it has to come from the environement, or that the network is missing a reaction somewhere to produce it.

    ```sh
    mene dead -d ebame/toys/metaorganism.sbml
    ```

- MenePath provides pathways of reactions that can be used to produce target metabolites. It works only for targets that *can* be produced (*cf* MeneCheck) and can be called with the option `--min` in order to display pathways of minimal size. There are often several possibilities to produce target metabolites; instead of enumerating all of them by default, the programme computes the intersection and union of all (minimal) pathways. It does not scale very well with the minimize option at genome-scale but let's take a look at an example.

    ```sh
    mene path -d ebame/toys/metaorganism.sbml -t ebame/toys/targets_individual.sbml -s ebame/toys/seeds_community.sbml --min
    ```

    The output tells us that a minimal pathway of reactions for the production of F has 5 reactions, that there is a unique solution (intersection has the same content as the given solution): R2, R3, R5, 10 and R16. For the production of C with a minimal pathway, there is also a unique solution made of 4 reactions: R1, R4, R9, R13.

### Using real networks <a name="metabolic_exploration_real"></a>

Now that we understand the basic concepts of the *scope* and of *network expansion* to assess the metabolic potential of a metabolic network from a set of available metabolites (nutrients, or *seeds*), we can dive into the exploration of real metabolic networks. 

We can look at the metabolism of *Mycoplasma genitalium* for which we reconstructed a network earlier. 
A first thing to consider is the presence of exchange reactions such as `1 M_glc__D_e <-->` or `--> 1 M_glc__D_e` (see above in the description of metabolic reactions in SBML) that implicitely define seeds by producing metabolic compounds from nothing. Metabolic reconstruction tools that produce ready-to-simulate models usually add those reactions as they enable the activation of an objective reaction (often the biomass). Carveme, gapseq are such tools. We can check this by computing the scope from an empty set of seed metabolites (`toys/seeds_empty.sbml` has a dummy metabolite that does not exist and therefore there are no real seeds in the simulations using this file). 

```sh
mene scope -s ebame/toys/seeds_empty.sbml -d ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1.xml
```

shows a scope of 182 metabolites, meaning that there are implicit seeds in the metabolic network. You observe that the tool generates warnings when it encounters reactions without reactants or without products. Those are the reactions that are susceptible to be import reactions for implicit seeds. 

MeneSeed identifies the reactions that have no reactant, and the reversible reactions that have no product, and will therefore generate compounds considered as seeds by the network expansion algorithm. 

```sh
mene seed -d ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1.xml
```

There are 102 of them.
In contrast, running this programme on the draft network of *M. genitalium* only highlights 69 transport reactions of metabolites, meaning that in the ultimate step of metabolic reconstruction, additional transport reactions were added for simulation purpose.

```sh
mene seed -d ebame/data/Mycoplasma_genitalium/gapseq_outputs/GCF_000027325.1-draft.xml
```

In `data/oceanDNA/metabolic_networks` are located metabolic networks constructed from MAGs associated selected in the OceanDNA MAG catalogue [19]. The associated metadata associated to each MAG is detailed in `data/oceanDNA/selected_genomes.tsv`. Metabolic networks were constructed either with Carveme, or with Pathway Tools. The metabolic networks may be compressed in case you did not run previous parts of the tutorial. In that case, you can use `gunzip` to uncompress them. Otherwise, you should have `.sbml` files in the directory, and not `.sbml.gz`. 

```sh
for elem in ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b*.gz ; do gunzip $elem ; done
```

Let's check the number of metabolites in the scope using empty seeds as above:

```sh
for elem in ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b*.sbml ; do nm=$(echo $elem | cut -d / -f7 | cut -d. -f1) && echo "- $nm:" && mene scope -s ebame/toys/seeds_empty.sbml -d $elem | grep "on scope" ; done
```

Carveme metabolic networks produces non-empty scopes: seeds are considered.
With this command, we computed the scope for each metabolic network constructed with Carveme, using the seeds defined in the SBML. Between 267 and 477 metabolites are predicted to be producible.

In the case of Carveme reconstruction, the metabolic network is a simulation-ready model (and has import reactions for seeds), whereas in the Pathway Tools generated networks, no such reactions exist.

This can be verified using this bash command:

```sh
for elem in ebame/data/oceanDNA/metabolic_networks/pathwaytools/sbml/*.sbml.gz; do cp $elem . && nm=$(echo $elem | cut -d / -f7 | cut -d. -f1) && gunzip $nm.sbml.gz && echo "- $nm:" && mene scope -s ebame/toys/seeds_empty.sbml -d $nm.sbml | grep "on scope" && rm $nm.sbml ; done
```

This command loops over all SBML stored in the Pathway Tool directory, copies it where you are, unzips it, call MeneScope on it with the dummy seeds file, retains only the line of output with the number of produced metabolites and removed the unzipped file.
You see that the scopes are always empty.

A growth medium compatible with Pathway Tools identifiers is located in `data/oceanDNA/seeds_ocean_dna_ptools.sbml`. You can try it with MeneScope on some metabolic networks.

```sh
# copy the compressed file
cp ebame/data/oceanDNA/metabolic_networks/pathwaytools/sbml/OceanDNA-b13538.sbml.gz .
# uncompress it
gunzip OceanDNA-b13538.sbml.gz
# run the scope computation
mene scope -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml -d OceanDNA-b13538.sbml
```

376 metabolites are producible using the seeds.

Note that Pathway Tools identifiers are encoded in the SBML file, and thus in the outputs from the tools. For instance `M_CPD__45__15382_c` would be found in the database as `CPD-15382`. If you feel like coding a bit, a way to automatise the "translation" is the following, you can use this snippet in a python script.

A nice python interpreter installed in the environment is `ptpython``(`pip install ptpython`). Launch it writing `ptpython` in the console.
Then:

```python 
from padmet.utils.sbmlPlugin import convert_from_coded_id, convert_to_coded_id
# let's uncode one ID
coded_id = "M_CPD__45__15382_c"
uncoded, object_type, compart = convert_from_coded_id(coded_id)
print(uncoded)
# let's code it again for fun but this time as an extracellular compound
coded_again = convert_to_coded_id(uncoded, _type="M", compart="e")
print(coded_again)
# M stands for metabolite. If we wanted to code a reaction, the prefix would be "R". 
# Let's encode dummy reaction "RXN--+1673-ji"
print(convert_to_coded_id("RXN--+1673-ji", "R"))
```

In order to gain more information on compounds or reactions of the metabolic networks:

- for Carveme reconstructions, go to the [BIGG database](https://bigg.ucsd.edu/)
- for Pathway Tools reconstructions, go to the [MetaCyc database](https://metacyc.org/)

## Screening the metabolism of microbial communities <a name="metabolic_exploration_com"></a>

Network expansion-like algorithms can be used to assess the metabolic potential of communities [20], by taking into account that metabolites produced by a species can benefit other species. This is similar to considering a meta-organism obtained by merging all individual metabolic networks, although in this case, we keep track of the boundaries between species and identify which metabolites would be exchanged. Once again, we will start with toy metabolic networks in order to understand the concepts, then we'll switch to the real data.

### Using toy community data <a name="metabolic_exploration_com_toy"></a>

For this part, we will work with a collection of 6 toy metabolic networks, stored as SBML files in `toys/6_bact_community`. The script that was used to generate these files is in `scripts/community_creation_sbml.py`.

<img src="figures/comm_6_bact.png" alt="A representation of six small metabolic networks described as files in the six SBML files of `toys/6_bact_community`" title="A representation of six small metabolic networks described as files in the six SBML files of `toys/6_bact_community`" width="400"/>  

If we compare the content of `metaorganism.sbml` and the 6 individual metabolic networks, we notice that the former is actually the merged metabolism of the community. A meta-organism approach can be useful for screening the metabolism of an assembled metagenome in cased few MAGs are recovered. Starting from the non redundant gene catalogue of the metagenome, one can reconstruct the metabolic network and perform simulation in order to obtain the metabolic potential of the ecosystem. While the boundaries between species will not be known and neither the predicted producers of molecules, knowing the range of metabolic functions associated to the ecosystem can be valuable. 

Here will use [MiSCoTo][https://github.com/cfrioux/miscoto] [20] and [Metage2Metabo](https://github.com/AuReMe/metage2metabo/) [21] to explore the metabolic networks of our small community.

MiSCoTo is a tool that explores the metabolic potential of microbial communities and enables the selection of minimal communities harbouring a behaviour of interest, i.e. producing a set of targeted compounds. It can use a host metabolic network in order to measure the impact of the microbiota metabolism on the host metabolism. Here we will only use symbionts.

MiSCoTo_scopes computes the community scope of the ecosystem. It takes as inputs a directory of metabolic networks and a file describing the seeds.

```sh
miscoto scopes -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml 
```

It can also take a file describing targets in order to check their producibility by the community.

```sh
miscoto scopes -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml -t ebame/toys/targets_community.sbml
```

Here, 13 metabolites are producible in an interacting community composed of the 6 metabolic networks, among which the two targets, C and F.

MiSCoTo_focus aims at identifying the producers of metabolites in the community. Indeed, the scope above does not inform on the bacteria responsible for the production of each compound.

We can call this programme using a symbiont of interest we want to focus on:

```sh
miscoto focus -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml --focus bact6  --output ebame/results/test_focus.json
```

For this you have to pick the basename of the SBML file, for instance `bact6`.
Alone in the described environment (S1 and S2) bact6 cannot produce anything. However, if interacting with the other 5, it can produce 3 metabolites.

Or we can run the analysis for every symbiont of the community:

```sh
miscoto focus  -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml  --all --output ebame/results/test_focus.json
```

By doing so, we observe that bact3 is the symbiont that beneficiates the most from interactions with others: it can produce 4 new metabolites: E, H, G and N.

When screening the metabolic potential of a community, one can wonder which sub-community, of a less complex composition, could perform a function of interest. For instance in this example, do we need the 6 bacteria to produce the compounds C and F?

MiSCoTo_mincom addresses this question by solving a combinatorial optimisation problem. It has 2 modes:

- a mode "soup" that only aims at minimizing the number of symbionts in the selected community

    ```sh
    miscoto mincom -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml -t ebame/toys/targets_community.sbml -o soup
    ```

    There are several options to the programme:

    - by default a unique solution will be computed (equivalent to  `--optsol` option). There might be others however, and possibly a lot of them
    - `--enumeration` gives the entire set of solutions. It can take a while to compute for large communities
    - `--union` computes the union of solutions, i.e. lists all metabolic networks that occur in at least one minimal size community producing the target compounds
    - `--intersection` computes the intersection of solutions, i.e. lists all metabolic networks that occur in EVERY minimal size community producing the targets. 

    Play with the options. There are 4 equivalent minimal size communities, each comprising 3 bacteria. 5 bacteria out of the initial 6 occur in some solutions. One symbiont, bact1, occurs in every minimal community producing C and F.

Below is a figure describing the minimal solutions and the minimal exchanges they would require to produce the targets. 

<img src="figures/toy_community_solutions.png" alt="A graphical description of the four minimal-size communities enabling the production of C and F target metabolites. Solution 1: bact1, bact2, bact5 ; minimal exchanges include: D from bact1 to bact5, N from bact2 to bact5, C from bact5 to bact2 and E from bact2 to bact1. Solution2: bact1, bact2, bact6; minimal exchanges include: D from bact1 to bact6, N from bact2 to bact6, C from bact6 to bact2 and E from bact2 to bact1. Solution3: bact1, bact3, bact5; minimal exchanges include: R from bact1 to bact3, D from bact1 to bact5, N from bact3 to bact5, K from bact5 to bact3, E from bact3 to bact1. Solution 4: bact1, bact3, bact6; minimal exchanges include: R from bact1 to bact3, R from bact1 to bact6, D from bact1 to bact6, N from bact3 to bact6, K from bact6 to bact3, E from bact3 to bact1." title="A graphical description of the four minimal-size communities enabling the production of C and F target metabolites together with the minimal exchanges they imply (in purple)" width="800"/>


- a mode "minexch" that additionally tries to minimize to minimize the number of exchanges within the community

    ```sh
    miscoto mincom -b ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml -t ebame/toys/targets_community.sbml -o minexch
    ```

    The above listed options also work with the "minexch" mode.

    When minimizing the need for metabolic exchanges, the optimal solution is also of size 3, and we learn that at least 4 exchanges of metabolites are needed. An enumeration of the solutions highlights that only 2 communities minimizing the number of bacteria and their number of exchanges exist. The intersection tells us that 2 symbionts always occur in these communities (bact1 and bact2) and that altogether, 4 bacteria occur in the minimal communities.

We've learned how to analyse individual metabolic networks and a community of them. By combining both we can infer the impact of metabolic interaction on the putative metabolic potential of species in a given environment. Metage2Metabo uses a subset of the above analysis to provide an all-in-one pipeline connecting MAGs or collection of reference genomes to metabolic predictions. The pipeline automatises the reconstruction of metabolic networks with Pathway Tools but you can skip this step and directly use your previously reconstructed networks. This is what we will do with our toy community. 

There are several commands (`m2m --help`), here we will use metacom, that performs all pipeline steps except the reconstruction.

```sh
m2m metacom -n ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml -o m2m_toy
```

Here is a description of what the pipeline does:

- First it computes the individual scope of each symbiont using the seeds provided. This is equivalent to running MeneScope on each metabolic network. It gives the core individual metabolic potential: metabolites producible individually by all symbionts, and the pan-metabolic potential, i.e. the union of all individually producible metabolites. The min and max, and average sizes of individual scopes are also provided. In addition to the printed logs, `m2m_toy/indiv_scopes/` holds several files.
  - `m2m_toy/indiv_scopes/indiv_produced_seeds.json` gives the seeds that can be produced by each symbiont, if any
  - `m2m_toy/indiv_scopes/indiv_scopes.json` lists the compounds in the individual scope of each symbiont in json format
  - `m2m_toy/indiv_scopes/rev_iscope.json` is a reversed version of the previous dictionary: for each producible compound, which organism hold them in their scope
  - `m2m_toy/indiv_scopes/rev_iscope.tsv` is a matrix summarising the same information. A "1" means the compound is in the scope of the organism, a 0 means it's not.
- Then it computes the community scope of the community. This is equivalent to running MiSCoTO_scope. The community scope is not printed because it can be too large, and a folder is created in `m2m_toy/community_analysis/` gathering the results (`m2m_toy/community_analysis/comm_scopes.json`)
- Comparing the union of the individual scopes and the community scope provides the list of compounds that can only be produced if interactions occurs within the community. This is what is called the "added-value of cooperation". The compounds are depected in the logs and are listed in `m2m_toy/community_analysis/addedvalue.json`.
- By default, m2m will select a minimal community producing these newly producible compounds. A corresponding target file is created in `m2m_toy/community_analysis/targets.sbml`. MiSCoTo_mincom in soup mode is used for the computation. You can provide the call to the programme with a SBML file of targeted metabolites containing other molecules than the default ones. Here we can use `-t toys/targets_community.sbml` in the metacom call, but we could also create a target file containg all compounds of the community scope, in order to find a minimal community harbouring the same metabolic capabilities as the initial one. A dedicated `m2m seeds` command can create seed and target metabolite files using text files with compound identifiers. The minimal community selection steps provides one minimal community, and computes the union and intersection of all solutions. This enables the computation of two sets of symbionts: "essential symbionts" occurring in every minimal community (intersection), and "alternative symbionts", occurring in some but not all solutions (union - intersection). All of them form what is called the "key species", that occur in some or all minimal communities. In addition to the printed output, `m2m_toy/community_analysis/mincom.json` is created and contains the same information and additional one, such as the identification of target producers in the selected communities.

In MiSCoTo, we saw that we can enumerate all minimal solutions associated to the production of a set of target metabolites. By default, this is not done in M2M_metacom because the computation is quite heavy, contrary to the inferrence of one solution, or the union/intersection of solutions. It can be done however in M2M using the dedicated `m2m_analysis` pipeline.

We'll run it with our set of two target metabolites C, and F and will run the whole workflow

```sh
 m2m_analysis workflow -n ebame/toys/6_bact_community/ -s ebame/toys/seeds_community.sbml -t ebame/toys/targets_community.sbml -o ebame/results/m2m_analysis_toy --oog Oog.jar 
```

If an error is raised here `FileNotFoundError: [Errno 2] No such file or directory: 'm2m_analysis_toy/svg/targets_community.bbl.svg'`, just remove the `--oog Ooo.jar` part of the command: the programme complains because there is no X11 window server, which might happen on a server. 

Here is a description of the steps performed by M2M_analysis:

- First it enumerates all minimal size communities using MiSCoTo in soup mode. We do not count nor minimize the exchanges among symbionts. You get a summary of the key species in the logs, and files are created with the same information in `m2m_analysis_toy/json/targets_community.json`, and `m2m_analysis_toy/key_species.json` and `m2m_analysis_toy/miscoto_stats.txt`.
- Then a key species association graph is created, connected key species if they co-occur in one of the minimal communities. It is stored in `m2m_analysis_toy/gml/targets_community.gml`. You can visualise it using online tools, such as [https://www.yworks.com/yed-live/](https://www.yworks.com/yed-live/) and [http://graphml.com](http://graphml.com) [22], in which you can import and drag and drop the `.gml` file respectively.

    <img src="figures/toy_association_graph.png" alt="A representation of the association graph depicting the connections among key species occurring in minimal communities. The illustrated graph is described as graphml in `m2m_analysis_toy/gml/targets_community.gml`. The figure is generated with yworks.com" title="A representation of the association graph depicting the connections among key species occurring in minimal communities. The illustrated graph is described as graphml in `m2m_analysis_toy/gml/targets_community.gml`. The figure is generated with yworks.com" width="300"/>

- In larger communities, due to the redundance of metabolic functions in microbiomes, the number of keys species increases a lot and the number of minimal communities even more. This makes the association graph very dense and hard to decipher. This is where graph compression can come handy. [Power graph analysis](https://en.wikipedia.org/wiki/Power_graph_analysis) compresses [cliques, bicliques and star motifs](https://en.wikipedia.org/wiki/Power_graph_analysis#/media/File:PowerGraphs.png), creating aggregates of nodes called power nodes. The final step of M2M_analysis is the compression of the association graph into a power graph [26]. There is not a unique decomposition in power graphs (cf the wikipedia article), here we used [powergrASP](https://github.com/Aluriak/PowerGrASP) for their generation and we create a bbl visualisation (`m2m_analysis_toy/bbl/targets_community.bbl`) that can be visualised with [Cytoscape](https://github.com/Aluriak/PowerGrASP#bubble-files-usage) [23] [24], a html page with the visualisation that you can open in your browser (`m2m_analysis_toy/html/targets_community/index.html`), and a svg file if you provide the tool with the [downloadable](https://github.com/AuReMe/metage2metabo/tree/master/external_dependencies) `Oog.jar` file.
  
    <img src="figures/toy_powergraph.png" alt="The power graph associated to the minimal communities producing C and F and exported from the svg visualisation provided by m2m_analysis. The bact1 essential symbiont is in red. The four alternative symbiont - bact2, bact3, bact5 and bact6 - are in blue. Bact5 and Bact6 are in a power node, so are bact3 and bact2. Bact1 node is connected to bact2-bact3 power node and the three are included in another power node connected to bact5-bact6. With a bit of training we can read using this graph the composition of all minimal communities: bact2 and bact3 have equivalent roles, so have bact5 and bact6. A minimal community always includes bact1, and one member among bact2/bact3, as well as one among bact5/bact6." title="Power graph associated to the minimal communities producing C and F. The bact1 essential symbiont is in red. The four alternative symbiont are in blue. With a bit of training we can read using this graph the composition of all minimal communities: bact2 and bact3 have equivalent roles, so have bact5 and bact6. A minimal community always includes bact1, and one member among bact2/bact3, as well as one among bact5/bact6." width="300"/>

Now let's go back to the metabolic networks associated to the OceanDNA MAGs. 

### Using realistic communities <a name="metabolic_exploration_com_real"></a>

First let's copy and uncompress the data:
```sh
DIR="ebame/results/ocean_community"
mkdir $DIR
for elem in ebame/data/oceanDNA/metabolic_networks/pathwaytools/sbml/*.sbml.gz; do cp $elem $DIR && nm=$(echo $elem | cut -d / -f7 | cut -d. -f1) && gunzip $DIR/$nm.sbml ; done
```

Now, we can run MiSCoTo and compute the community scope of the oceanDNA ecosystem.

```sh
miscoto scopes -b $DIR -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml
```

We can do the same for the Carveme reconstructions, using the dummy seeds are there are already implicit seeds in the models due to exchange reactions.  

```sh
miscoto scopes -b ebame/data/oceanDNA/metabolic_networks/carveme/sbml/ -s ebame/toys/seeds_empty.sbml
```

We observe that the size of the community scope with Carveme is much bigger, certainly due to the high number of exchange reactions. To properly compare the two sets of reconstructions, we could delete the exchange reactions in the Carveme reconstructions and run the tools on them using the same set of seeds as for Pathway Tools. You may have noticed however that the identifiers are incompatible between both. So we would need to "translate" the MetaCyc seeds into the Bigg database. This is a bit cumbersome but you can see on the webpage of a compounds the connection to other databases ([see this example](https://metacyc.org/compound?orgid=META&id=L-ALPHA-ALANINE)). In practise, we could automatise the translation process as the Metacyc Database can be [dumped](https://www.metacyc.org/download.shtml). Alternatives include the use of [MetaNetX](https://www.metanetx.org/mnxdoc/mnxref.html) [25], an initiative that tries to reconcile metabolic databases (`chem_xref.tsv` has such translation data).

We can call MiSCoTo_focus using a symbiont of interest we want to focus on and see the impact of metabolic complementarity with other metabolic networks on its own metabolic potential:


Use MiSCoTo_focus:

```sh
miscoto focus -b ebame/results/ocean_community/ -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml --focus OceanDNA-b13538  --output ebame/results/test_focus.json
```

Let's now run metage2metabo and perform a full screening of the metabolic potential in the community.

```sh
m2m metacom -n ebame/results/ocean_community/ -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml -o m2m_ocean
```

Take a look at the results, get help from the description above.
116 metabolites are predicted to be producible only if metabolic interactions are allowed among the bacteria. 

We'll take a subset of these compounds to create a new target file: 

```txt
M_3__45__KETO__45__ADIPATE_c
M_CPD__45__19757_c
M_CPD__45__687_c
M_CPD__45__9189_c
M_CPD__45__9409_c
```

Create a `custom_targets.txt` file with these compounds listed inside, using `vi` or another text editor into `ebame/results/` directory.

> **VIM example:**
>
> To create a file, first run the following command
>
> `vim ebame/results/custom_targets.txt`
>
> Then copy and paste into the terminal the previous target list
>
> Finally press "escape" keyboard and then tap `:wq` to write and quit



Let's use `m2m seeds` to create the corresponding SBML file. It can create seeds and target SBML files. Both have the same structure, we'll just rename it. 

```sh
m2m seeds --metabolites ebame/results/custom_targets.txt -o ebame/results/custom_targets
# rename the file
mv ebame/results/custom_targets/seeds.sbml ebame/results/custom_targets/custom_targets.sbml
```

Now let's look at the selection of communities for the production of these targets.

```sh
m2m mincom -n ebame/results/ocean_community/ -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml -o ebame/results/m2m_ocean_custom_targets  -t ebame/results/custom_targets/custom_targets.sbml
```

6 out of the 10 initial metabolic networks occur in minimal communities, among which 2 occur in all of them.

Let's run m2m_analysis on it, note that we use the taxonomy of these MAGs in the analysis.

```sh
m2m_analysis workflow -n ebame/results/ocean_community/ -s ebame/data/oceanDNA/seeds_ocean_dna_ptools.sbml -o ebame/results/m2m_analysis_ocean_custom_targets  -t ebame/results/custom_targets/custom_targets.sbml --taxon ebame/data/oceanDNA/taxon_id.tsv --oog Oog.jar  --level phylum
```

Observe the generated power graphs and figure out which bacteria have equivalent roles in the construction of minimal communities. 

<img src="figures/oceanDNA_association_powergraph.png" alt="The power graph associated to the minimal communities producing the custom targets and exported from the svg visualisation provided by m2m_analysis. Among the two visualisations, we chose the one depicting the taonomy at the phylum level. The two essential symbionts are from the same phylum and are drawn in blue. Each of them is connected to a power node containing two nodes of key species each. Each combination of an essential species + the power node of two alternative symbiont are included in another power node. Finally these last 2 are connected together by a power edge. Based on this, we can infer that each minimal community consists of the two essential symbionts and one symbiont among each of the two internal power nodes." title="Power graph associated to the minimal communities producing the custom targets. Colour depict phylum-level taxonomy. We can infer that each minimal community consists of the two essential symbionts and one symbiont among each of the two internal power nodes."  width="300"/>

If you arrived at this step and understood everything, congrats. If you didn't understand everything, I may be the one to blame. 

Try selecting minimal communities producing other targets. 

If you want to see more complex power graphs, go the [m2m documentation](https://metage2metabo.readthedocs.io/en/latest/m2m_analysis.html#m2m-analysis-enum) or the [m2m paper](https://elifesciences.org/articles/61968). Also consider [M2M's tutorial](https://github.com/AuReMe/metage2metabo/blob/main/tutorials/method_tutorial/m2m_tutorial.ipynb)

# Inferring seed metabolites (nutrients) from the metabolic network

Reverse ecology aims at predicting the interaction between an organism and its environment through the analysis of its metabolic network. In particular, attention is given to the use of metabolic networks to predict the environment of the organism [27]. 

Here we use a similar formalism to the network expansion used to predict the reachable metabolites from seeds but solve the inverse problem: which seed compounds would be needed for a metabolic network to reach a function of interest? Again, we will illustrate the approach using a toy model and real data. We will use [Seed2LP](https://github.com/bioasp/seed2lp) for the inference of seed compounds [28].

## Using toy data

Let's first activate the conda environment.

```
conda activate ebame_metabo_reasoning
```

And install the tool.

```
pip install git+https://github.com/bioasp/seed2lp
```

We will work again with `ebame/toys/metaorganism.sbml`.

<img src="figures/metaorg.png" alt="A representation of a toy metabolic network that is also described as a SBML file in `toys/metaorganism.sbml`" title="A representation of a toy metabolic network that is also described as a SBML file in `toys/metaorganism.sbml`" width="400"/>  

We will try to suggest seeds for the production of `C` and `F`, listed in the file `ebame/seed_detection/toy_targets.txt`.


```
seed2lp target ebame/toys/metaorganism.sbml ebame/results/seed2lp_out/ -tf ebame/seed_detection/toy_targets.txt -so reasoning -nbs 10 -m subsetmin
```

The programme takes as input a metabolic network, a directory for writing the results. Here we use the mode `target`: we want to reach a set of targeted metabolites. We use the reasoning mode (network expansion), require at most 10 distinct solutions of size subset minimal (i.e. for a given suitable solution, there is no other solution that is a subset of the first one).

The objective here being the production of `C`and `F`, there are two subset-minimal solutions: {B, S1} and {S1,S2}.

```
Answer: 1 (2 seeds)
M_B_c, M_S1_c

Answer: 2 (2 seeds)
M_S1_c, M_S2_c
```

Adding a third targeted compound, `Z`(`ebame/seed_detection/toy_targets.txt seed_detection/toy_targets_2.txt`) generates new solutions including `M` as a seed, which makes sense because only `M` can reach the isolated `Z` metabolite in the network.

```
seed2lp target ebame/toys/metaorganism.sbml ebame/results/seed2lp_out/ -tf ebame/seed_detection/toy_targets_2.txt -so reasoning -nbs 10 -m subsetmin
```

## Using real networks

Let us work again with a metabolic network generated by Carveme. 
By default, the objective is to reach the biomass reaction, which is the objective function set up in the SBML file. 

> **WARNING** : Carveme sbml is normally already unzipped into `data/oceanDNA/metabolic_networks/carveme/sbml` if you followed the complete tutorial, see section **"Using real networks"** line 516 - 517. 
>
>If not already unzipped, run the following command :
>
> ```
> gunzip ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml.gz
> ```

And run Seed2lp: by default it will remove all import reactions that natively import nutrients in the model. They can be kept using a dedicated option `-kir`

```
seed2lp target ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml ebame/results/seed2lp_out/ -so reasoning -nbs 1 -m subsetmin
```

In reasoning mode, 23 compounds as seed are enough to reach the biomass reactants using network expansion.

If we keep the import reactions, only 6 seeds are necessary because compounds with exchange reactions freely diffuse in the network:

```
seed2lp target ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml ebame/results/seed2lp_out/ -so reasoning -nbs 1 -m subsetmin -kir
```

Should we want to keep track of those exchangeable compound and count them as seeds, we can use the option `ti`:

```
seed2lp target ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml ebame/results/seed2lp_out/ -so reasoning -nbs 1 -m subsetmin -kir -ti
```

There are now 161 seeds because many compounds were freely exchangeable in the SBML-encoded medium.

In the metabolic modeling field, many modellers use numerical models (see next section) that quantify the metabolic activity. Seed2LP combines network expansion with such a numerical model, Flux Balance Analysis, in order to find solutions that ensure both the discrete reachability of targets, and a positive flux in the objective reaction (biomass). Let us try it with the `filter` solving mode, that tests flux for all solutions raised by the reasoning model until one that satisfies the numerical model is found. 

```
seed2lp target ebame/data/oceanDNA/metabolic_networks/carveme/sbml/OceanDNA-b15250.sbml ebame/results/seed2lp_out/ -so filter -nbs 1 -m subsetmin
```

We obtain a solution of 24 seeds. The solver indicates that 10 solutions were rejected before this one because they did not satisfy the numerical constraints. 

Additional use-case of Seed2LP include:

- forbidding several metabolites to be included in seeds `-fsf`
- force seed selection to occur among a subset of pre-defined molecules `-psf`
- forcing some compounds to be seeds `-sf`
- Other solving options satisfying both network expansion and flux balance analysis
- Enabling some target metabolites to be selected as seed compounds (trivial solutions) `-tas`
- And others...

You can have a coffee to reward yourself for reaching the end of the tutorial.

# Numerical models of metabolism

If you want to go further, take a look at the numerical simulation of metabolic models using Flux Balance Analysis [29].

There are good online resources on the subject so there is no detailed section about it in this tutorial. 
Open a Python console (I suggest `ptpython`, `pip install ptpython`) and manipulate metabolic models (for instance, the core metabolism of _E. coli_) using [cobrapy](https://cobrapy.readthedocs.io/en/latest/index.html), following [this tutorial](https://cobrapy.readthedocs.io/en/latest/getting_started.html) and then [this one](https://cobrapy.readthedocs.io/en/latest/simulating.html).

## References

- <a id="1">[1]</a>
Frioux, C., Singh, D., Korcsmaros, T., and Hildebrand, F. (2020). From bag-of-genes to bag-of-genomes: metabolic modelling of communities in the era of metagenome-assembled genomes. Computational and Structural Biotechnology Journal, 18, 1722–1734. <https://doi.org/10.1016/j.csbj.2020.06.028>
- <a id="2">[2]</a>
Hucka, M., Finney, A., Sauro, H. M., Bolouri, H., Doyle, J. C., Kitano, H., Arkin, A. P., Bornstein, B. J., Bray, D., Cornish-Bowden, A., Cuellar, A. A., Dronov, S., Gilles, E. D., Ginkel, M., Gor, V., Goryanin, I. I., Hedley, W. J., Hodgman, T. C., Hofmeyr, J. H., … Wang, J. (2003). The systems biology markup language (SBML): A medium for representation and exchange of biochemical network models. Bioinformatics, 19(4), 524--531. <https://doi.org/10.1093/bioinformatics/btg015>
- <a id="3">[3]</a>
Hari, A., and Lobo, D. (2020). Fluxer: a web application to compute, analyze and visualize genome-scale metabolic flux networks. Nucleic Acids Research, 48(W1), gkaa409-. <https://doi.org/10.1093/nar/gkaa409>
- <a id=4>[4]</a>
Norsigian, C. J., Pusarla, N., McConn, J. L., Yurkovich, J. T., Dräger, A., Palsson, B. O., and King, Z. (2019). BiGG Models 2020: multi-strain genome-scale models and expansion across the phylogenetic tree. Nucleic Acids Research. <https://doi.org/10.1093/nar/gkz1054>
- <a id=5>[5]</a>
Machado, D., Andrejev, S., Tramontano, M., and Patil, K. R. (2018). Fast automated reconstruction of genome-scale metabolic models for microbial species and communities. Nucleic Acids Research, 46(15), 7542–7553. <https://doi.org/10.1093/nar/gky537>
- <a id=6>[6]</a>
Zimmermann, J., Kaleta, C., and Waschina, S. (2021). gapseq: informed prediction of bacterial metabolic pathways and reconstruction of accurate metabolic models. Genome Biology, 22(1), 81. <https://doi.org/10.1186/s13059-021-02295-1>
- <a id=7>[7]</a>
Karp, P. D., Midford, P. E., Billington, R., Kothari, A., Krummenacker, M., Latendresse, M., Ong, W. K., Subhraveti, P., Caspi, R., Fulcher, C., Keseler, I. M., and Paley, S. M. (2019). Pathway Tools version 23.0 update: software for pathway/genome informatics and systems biology. Briefings in Bioinformatics. <https://doi.org/10.1093/bib/bbz104>
- <a id=8>[8]</a>
Arkin, A. P., Cottingham, R. W., Henry, C. S., Harris, N. L., Stevens, R. L., Maslov, S., Dehal, P., Ware, D., Perez, F., Canon, S., Sneddon, M. W., Henderson, M. L., Riehl, W. J., Murphy-Olson, D., Chan, S. Y., Kamimura, R. T., Kumari, S., Drake, M. M., Brettin, T. S., … Yu, D. (2018). KBase: The United States Department of Energy Systems Biology Knowledgebase. Nature Biotechnology, 36(7), 566–569. <https://doi.org/10.1038/nbt.4163>
- <a id=9>[9]</a>
Karp, P. D., Billington, R., Caspi, R., Fulcher, C. A., Latendresse, M., Kothari, A., Keseler, I. M., Krummenacker, M., Midford, P. E., Ong, Q., Ong, W. K., Paley, S. M., and Subhraveti, P. (2019). The BioCyc collection of microbial genomes and metabolic pathways. Briefings in Bioinformatics, 20(4), 1085–1093. <https://doi.org/10.1093/bib/bbx085>
- <a id=10>[10]</a>
Noronha, A., Modamio, J., Jarosz, Y., Guerard, E., Sompairac, N., Preciat, G., Daníelsdóttir, A. D., Krecke, M., Merten, D., Haraldsdóttir, H. S., Heinken, A., Heirendt, L., Magnúsdóttir, S., Ravcheev, D. A., Sahoo, S., Gawron, P., Friscioni, L., Garcia, B., Prendergast, M., … Thiele, I. (2018). The Virtual Metabolic Human database: integrating human and gut microbiome metabolism with nutrition and disease. Nucleic Acids Research, 47(D1), D614–D624. <https://doi.org/10.1093/nar/gky992>
- <a id=11>[11]</a>
Heinken, A., Hertel, J., Acharya, G., Ravcheev, D. A., Nyga, M., Okpala, O. E., Hogan, M., Magnúsdóttir, S., Martinelli, F., Nap, B., Preciat, G., Edirisinghe, J. N., Henry, C. S., Fleming, R. M. T., and Thiele, I. (2023). Genome-scale metabolic reconstruction of 7,302 human microorganisms for personalized medicine. Nature Biotechnology, 1–12. <https://doi.org/10.1038/s41587-022-01628-0>
- <a id=12>[12]</a>
Malik-Sheriff, R. S., Glont, M., Nguyen, T. V. N., Tiwari, K., Roberts, M. G., Xavier, A., Vu, M. T., Men, J., Maire, M., Kananathan, S., Fairbanks, E. L., Meyer, J. P., Arankalle, C., Varusai, T. M., Knight-Schrijver, V., Li, L., Dueñas-Roca, C., Dass, G., Keating, S. M., … Hermjakob, H. (2019). BioModels—15 years of sharing computational models in life science. Nucleic Acids Research, 48(D1), D407–D415. <https://doi.org/10.1093/nar/gkz1055>
- <a id=13>[13]</a>
Hyatt, D., Chen, G.-L., LoCascio, P. F., Land, M. L., Larimer, F. W., and Hauser, L. J. (2010). Prodigal: prokaryotic gene recognition and translation initiation site identification. BMC Bioinformatics, 11(1), 119. <https://doi.org/10.1186/1471-2105-11-119>
- <a id=14>[14]</a>
Cantalapiedra, C. P., Hernández-Plaza, A., Letunic, I., Bork, P., and Huerta-Cepas, J. (2021). eggNOG-mapper v2: Functional Annotation, Orthology Assignments, and Domain Prediction at the Metagenomic Scale. Molecular Biology and Evolution, 38(12), msab293-. <https://doi.org/10.1093/molbev/msab293>
- <a id=15>[15]</a>
Kanehisa, M., Furumichi, M., Sato, Y., Ishiguro-Watanabe, M., and Tanabe, M. (2021). KEGG: integrating viruses and cellular organisms. Nucleic Acids Research, 49(D1), gkaa970-. <https://doi.org/10.1093/nar/gkaa970>
- <a id=16>[16]</a>
Aite, M., Chevallier, M., Frioux, C., Trottier, C., Got, J., Cortés, M. P., Mendoza, S. N., Carrier, G., Dameron, O., Guillaudeux, N., Latorre, M., Loira, N., Markov, G. V., Maass, A., and Siegel, A. (2018). Traceability, reproducibility and wiki-exploration for “à-la-carte” reconstructions of genome-scale metabolic models. PLOS Computational Biology, 14(5), e1006146. <https://doi.org/10.1371/journal.pcbi.1006146>
- <a id=17>[17]</a>
Ebenhöh, O., Handorf, T., and Heinrich, R. (2004). Structural analysis of expanding metabolic networks. Genome Informatics. International Conference on Genome Informatics, 15(1), 35–45.
- Nishimura, Y., and Yoshizawa, S. (2022). The OceanDNA MAG catalog contains over 50,000 prokaryotic genomes originated from various marine environments. Scientific Data, 9(1), 305. <https://doi.org/10.1038/s41597-022-01392-5>
- <a id=18>[18]</a>
Ebrahim, A., Lerman, J. A., Palsson, B. O., and Hyduke, D. R. (2013). COBRApy: COnstraints-Based Reconstruction and Analysis for Python. BMC Systems Biology, 7(1), 74. <https://doi.org/10.1186/1752-0509-7-74>
- <a id=19>[19]</a>
Nishimura, Y., and Yoshizawa, S. (2022). The OceanDNA MAG catalog contains over 50,000 prokaryotic genomes originated from various marine environments. Scientific Data, 9(1), 305. <https://doi.org/10.1038/s41597-022-01392-5>
- <a id=20>[20]</a>
Frioux, C., Fremy, E., Trottier, C., and Siegel, A. (2018). Scalable and exhaustive screening of metabolic functions carried out by microbial consortia. Bioinformatics, 34(17), i934–i943. <https://doi.org/10.1093/bioinformatics/bty588>
- <a id=21>[21]</a>
Belcour, A., Frioux, C., Aite, M., Bretaudeau, A., Hildebrand, F., and Siegel, A. (2020). Metage2Metabo, microbiota-scale metabolic complementarity for the identification of key species. eLife, 9, e61968. <https://doi.org/10.7554/elife.61968>
- <a id=22>[22]</a>
Rossi, R. A., and Ahmed, N. K. (n.d.). The Network Data Repository with Interactive Graph Analytics and Visualization. Proceedings of the Twenty-Ninth AAAI Conference on Artificial Intelligence. <http://networkrepository.com>
- <a id=23>[23]</a>
Shannon, P., Markiel, A., Ozier, O., Baliga, N. S., Wang, J. T., Ramage, D., Amin, N., Schwikowski, B., and Ideker, T. (2003). Cytoscape: A Software Environment for Integrated Models of Biomolecular Interaction Networks. Genome Research, 13(11), 2498–2504. <https://doi.org/10.1101/gr.1239303>
- <a id=24>[24]</a>
Bourneuf, L., and Nicolas, J. (2017). Formal Concept Analysis, 14th International Conference, ICFCA 2017, Rennes, France, June 13-16, 2017, Proceedings. 89–105. <https://doi.org/10.1007/978-3-319-59271-8_6>
- <a id=25>[25]</a>
Moretti, S., Tran, V. D. T., Mehl, F., Ibberson, M., and Pagni, M. (2020). MetaNetX/MNXref: unified namespace for metabolites and biochemical reactions in the context of metabolic models. Nucleic Acids Research, 49(D1), gkaa992-. <https://doi.org/10.1093/nar/gkaa992>
- <a id=26>[26]</a>
Royer, L., Reimann, M., Andreopoulos, B., and Schroeder, M. (2008). Unraveling Protein Networks with Power Graph Analysis. PLoS Computational Biology, 4(7), e1000108. <https://doi.org/10.1371/journal.pcbi.1000108>
- <a id=27>[27]</a> Cao, Y., Wang, Y., Zheng, X., Li, F., and Bo, X. (2016). RevEcoR: an R package for the reverse ecology analysis of microbiomes. BMC Bioinformatics, 17(1), 294. <https://doi.org/10.1186/s12859-016-1088-4>
- <a id=28>[28]</a> Ghassemi-Nedjad, C., Bolteau, M., Bourneuf, L., Paulevé, L., and Frioux, C. (2024). Seed2LP: seed inference in metabolic networks for reverse ecology applications. <https://doi.org/10.1101/2024.09.26.615309> 
- <a id=29>[29]</a>
Orth, J. D., Thiele, I., and Palsson, B. Ø. (2010). What is flux balance analysis? Nature Biotechnology, 28(3), 245–248. <https://doi.org/10.1038/nbt.1614>
- <a id=30>[30]</a> Lieven, C., Beber, M. E., Olivier, B. G., Bergmann, F. T., Ataman, M., Babaei, P., Bartell, J. A., Blank, L. M., Chauhan, S., Correia, K., Diener, C., Dräger, A., Ebert, B. E., Edirisinghe, J. N., Faria, J. P., Feist, A. M., Fengos, G., Fleming, R. M. T., García-Jiménez, B., … Zhang, C. (2020). MEMOTE for standardized genome-scale metabolic model testing. Nature Biotechnology, 38(3), 272–276. <https://doi.org/10.1038/s41587-020-0446-y>

## Acknowledgements

- Chabname Ghassemi Nedjad (PhD student @ LaBRI, Inria, U. Bordeaux)
- Coralie Muller (Engineer @ Inria)
- Joseph Vallino for his useful comments and typo fixes
