# This script aims at constructing SBML files
# for a toy community of 6 organisms

from cobra import Model, Reaction, Metabolite
from cobra.io import write_sbml_model

collection = Model('collection')

# create the compartment c
collection.compartments['c'] = 'cytosol'

# Here are the reactions
# R1: S1 -> A
# R2: S2 -> B
# R3: E -> F
# R4: A -> D
# R5: B -> R
# R6: G -> H
# R7: M -> Z
# R8: C -> E + G
# R9: A -> N
# R10: B + K -> E + G
# R11: U -> V
# R12: A + R -> N
# R13: D + N -> C
# R14: C -> V
# R15: V + S1 -> K
# R16: R + S1 -> K
# R17: R -> H
# R18: H -> X

# Here are the metabolites
# S1, S2, A, B, E, F, D, R, G, H, M, Z, C, N, K, U, V, X

# Composition of the 6 models:
# Model 1: R1, R2, R3, R4, R5, R17
# Model 2: R1, R2, R6, R8, R9
# Model 3: R1, R2, R6, R7, R10, R12
# Model 4: R2, R7, R11, R18
# Model 5: R13, R14, R15
# Model 6: R13, R16, R18

# Creation of the metabolites in cobra
m_S1 = Metabolite('S1_c', name='S1', compartment='c')
m_S2 = Metabolite('S2_c', name='S2', compartment='c')
m_A = Metabolite('A_c', name='A', compartment='c')
m_B = Metabolite('B_c', name='B', compartment='c')
m_C = Metabolite('C_c', name='C', compartment='c')
m_D = Metabolite('D_c', name='D', compartment='c')
m_E = Metabolite('E_c', name='E', compartment='c')
m_F = Metabolite('F_c', name='F', compartment='c')
m_G = Metabolite('G_c', name='G', compartment='c')
m_H = Metabolite('H_c', name='H', compartment='c')
m_K = Metabolite('K_c', name='K', compartment='c')
m_M = Metabolite('M_c', name='M', compartment='c')
m_N = Metabolite('N_c', name='N', compartment='c')
m_R = Metabolite('R_c', name='R', compartment='c')
m_U = Metabolite('U_c', name='U', compartment='c')
m_V = Metabolite('V_c', name='V', compartment='c')
m_X = Metabolite('X_c', name='X', compartment='c')
m_Z = Metabolite('Z_c', name='Z', compartment='c')

# Creation of reactions
reaction_R1 = Reaction('R1')
reaction_R1.name = 'R1'
reaction_R1.add_metabolites({
    m_S1: -1.0,
    m_A: 1.0
})
reaction_R1.lower_bound = 0.0
reaction_R1.upper_bound = 1000.0
#
reaction_R2 = Reaction('R2')
reaction_R2.name = 'R2'
reaction_R2.add_metabolites({
    m_S2: -1.0,
    m_B: 1.0
})
reaction_R2.lower_bound = 0.0
reaction_R2.upper_bound = 1000.0
#
reaction_R3 = Reaction('R3')
reaction_R3.name = 'R3'
reaction_R3.add_metabolites({
    m_E: -1.0,
    m_F: 1.0
})
reaction_R3.lower_bound = 0.0
reaction_R3.upper_bound = 1000.0
#
reaction_R4 = Reaction('R4')
reaction_R4.name = 'R4'
reaction_R4.add_metabolites({
    m_A: -1.0,
    m_D: 1.0
})
reaction_R4.lower_bound = 0.0
reaction_R4.upper_bound = 1000.0
#
reaction_R5 = Reaction('R5')
reaction_R5.name = 'R5'
reaction_R5.add_metabolites({
    m_B: -1.0,
    m_R: 1.0
})
reaction_R5.lower_bound = 0.0
reaction_R5.upper_bound = 1000.0
#
reaction_R6 = Reaction('R6')
reaction_R6.name = 'R6'
reaction_R6.add_metabolites({
    m_G: -1.0,
    m_H: 1.0
})
reaction_R6.lower_bound = 0.0
reaction_R6.upper_bound = 1000.0
#
reaction_R7 = Reaction('R7')
reaction_R7.name = 'R7'
reaction_R7.add_metabolites({
    m_M: -1.0,
    m_Z: 1.0
})
reaction_R7.lower_bound = 0.0
reaction_R7.upper_bound = 1000.0
#
reaction_R8 = Reaction('R8')
reaction_R8.name = 'R8'
reaction_R8.add_metabolites({
    m_C: -1.0,
    m_E: 1.0,
    m_G: 1.0
})
reaction_R8.lower_bound = 0.0
reaction_R8.upper_bound = 1000.0
#
reaction_R9 = Reaction('R9')
reaction_R9.name = 'R9'
reaction_R9.add_metabolites({
    m_A: -1.0,
    m_N: 1.0
})
reaction_R9.lower_bound = 0.0
reaction_R9.upper_bound = 1000.0
#
reaction_R10 = Reaction('R10')
reaction_R10.name = 'R10'
reaction_R10.add_metabolites({
    m_B: -1.0,
    m_K: -1.0,
    m_E: 1.0,
    m_G: 1.0
})
reaction_R10.lower_bound = 0.0
reaction_R10.upper_bound = 1000.0
#
reaction_R11 = Reaction('R11')
reaction_R11.name = 'R11'
reaction_R11.add_metabolites({
    m_U: -1.0,
    m_V: 1.0
})
reaction_R11.lower_bound = 0.0
reaction_R11.upper_bound = 1000.0
#
reaction_R12 = Reaction('R12')
reaction_R12.name = 'R12'
reaction_R12.add_metabolites({
    m_A: -1.0,
    m_R: -1.0,
    m_N: 1.0
})
reaction_R12.lower_bound = 0.0
reaction_R12.upper_bound = 1000.0
#
reaction_R13 = Reaction('R13')
reaction_R13.name = 'R13'
reaction_R13.add_metabolites({
    m_D: -1.0,
    m_N: -1.0,
    m_C: 1.0
})
reaction_R13.lower_bound = 0.0
reaction_R13.upper_bound = 1000.0
#
reaction_R14 = Reaction('R14')
reaction_R14.name = 'R14'
reaction_R14.add_metabolites({
    m_C: -1.0,
    m_V: 1.0
})
reaction_R14.lower_bound = 0.0
reaction_R14.upper_bound = 1000.0
#
reaction_R15 = Reaction('R15')
reaction_R15.name = 'R15'
reaction_R15.add_metabolites({
    m_V: -1.0,
    m_S1: -1.0,
    m_K: 1.0
})
reaction_R15.lower_bound = 0.0
reaction_R15.upper_bound = 1000.0
#
reaction_R16 = Reaction('R16')
reaction_R16.name = 'R16'
reaction_R16.add_metabolites({
    m_R: -1.0,
    m_S1: -1.0,
    m_K: 1.0
})
reaction_R16.lower_bound = 0.0
reaction_R16.upper_bound = 1000.0
#
reaction_R17 = Reaction('R17')
reaction_R17.name = 'R17'
reaction_R17.add_metabolites({
    m_R: -1.0,
    m_H: 1.0
})
reaction_R17.lower_bound = 0.0
reaction_R17.upper_bound = 1000.0
#
reaction_R18 = Reaction('R18')
reaction_R18.name = 'R18'
reaction_R18.add_metabolites({
    m_H: -1.0,
    m_X: 1.0
})
reaction_R18.lower_bound = 0.0
reaction_R18.upper_bound = 1000.0

# Add reactions to the model
collection.add_reactions([reaction_R1, reaction_R2, reaction_R3, reaction_R4,
                            reaction_R5, reaction_R6, reaction_R7, reaction_R8,
                            reaction_R9, reaction_R10, reaction_R11, reaction_R12,
                            reaction_R13, reaction_R14, reaction_R15, reaction_R16,
                            reaction_R17, reaction_R18])

# Write the model in SBML format
with open("toys/metaorganism.sbml", "w") as f_sbml:
    write_sbml_model(collection, filename=f_sbml)

# create all 6 models
model1 = Model('bact1')
model1.compartments['c'] = 'cytosol'
model2 = Model('bact2')
model2.compartments['c'] = 'cytosol'
model3 = Model('bact3')
model3.compartments['c'] = 'cytosol'
model4 = Model('bact4')
model4.compartments['c'] = 'cytosol'
model5 = Model('bact5')
model5.compartments['c'] = 'cytosol'
model6 = Model('bact6')
model6.compartments['c'] = 'cytosol'

# add reations to each model
model1.add_reactions([reaction_R1, reaction_R2, reaction_R3, reaction_R4,
                            reaction_R5, reaction_R17])
model2.add_reactions([reaction_R1, reaction_R2, reaction_R6, reaction_R8,
                            reaction_R9])
model3.add_reactions([reaction_R1, reaction_R2, reaction_R6, reaction_R7,
                            reaction_R10, reaction_R12])
model4.add_reactions([reaction_R2, reaction_R7, reaction_R11, reaction_R18])
model5.add_reactions([reaction_R13, reaction_R14, reaction_R15])
model6.add_reactions([reaction_R13, reaction_R16, reaction_R18])

# Write the model in SBML format
with open("toys/6_bact_community/bact1.sbml", "w") as f_sbml:
    write_sbml_model(model1, filename=f_sbml)
with open("toys/6_bact_community/bact2.sbml", "w") as f_sbml:
    write_sbml_model(model2, filename=f_sbml)
with open("toys/6_bact_community/bact3.sbml", "w") as f_sbml:
    write_sbml_model(model3, filename=f_sbml)
with open("toys/6_bact_community/bact4.sbml", "w") as f_sbml:
    write_sbml_model(model4, filename=f_sbml)
with open("toys/6_bact_community/bact5.sbml", "w") as f_sbml:
    write_sbml_model(model5, filename=f_sbml)
with open("toys/6_bact_community/bact6.sbml", "w") as f_sbml:
    write_sbml_model(model6, filename=f_sbml)
